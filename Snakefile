configfile: "config/config.yaml"

inputs = []

def read_genomefile(f):
    import pandas as pd
    table = pd.read_table(f)
    return table

rule generate_reads:
    input:
        config["genome_file"]
    output:
        "{}/{}_{}_R1.fastq.gz".format(config["results_path"],os.path.basename(config["genome_file"].replace(".tab","")),config["reads"]),
        "{}/{}_{}_R2.fastq.gz".format(config["results_path"],os.path.basename(config["genome_file"].replace(".tab","")),config["reads"])
    params:
        length = config["length"],
        reads = config["reads"]
    run:
        import os
        import numpy as np
        table = read_genomefile(input[0])
        outdir = os.path.dirname(output[0])
        for i in table.index:
            zipped = False
            frac = float(table.iloc[i]["fraction"])
            if frac == 0.0:
                continue
            nreads = int(np.round(params.reads * frac))
            file = table.iloc[i]["file"]
            shell("randomreads.sh ref={file} out={outdir}/seqs_{i}_R#.fq.gz paired=true length={params.length} reads={nreads}")
        shell("gunzip -c {outdir}/seqs*R1.fq.gz | gzip -c > {output[0]}")
        shell("gunzip -c {outdir}/seqs*R2.fq.gz | gzip -c > {output[1]}")
        shell("rm -rf {outdir}/seqs*")

rule all:
    input:
        "{}/mock_{}_R1.gz".format(config["results_path"],config["reads"]),
        "{}/mock_{}_R2.gz".format(config["results_path"],config["reads"])