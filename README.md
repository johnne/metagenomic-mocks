# README #

### What is this repository for? ###

Workflow to create metagenomic mock communities from microbial genomes.

## How to run it

The workflow takes as input a file with species names path to genome
files and a fraction of reads to generate from each genome.

The `randomreads.sh` script from the [BBMap toolkit](https://github.com/BioInfoTools/BBMap)
is then used to generate random reads from each genome.

**Example**:

Generate a total of 10 000 (paired-end) reads from genomes specified in
file data/mock.tab.

```
snakemake --config reads=10000 genome_file=data/mock.tab
```

Resulting output files are stored under `results/` and are named:
`<genome_file_name>_<reads>_R#.fastq.gz`

### Genome files
The files **anteroid_nares.tab**, **buccal_mucosa.tab**, **retr_crease.tab**
and **stool.tab** that are found in `data/` have contain the same genomes
but in fractions somewhat representative of the results from
[Structure, function and diversity of the healthy human microbiome](https://www.nature.com/articles/nature11234).
